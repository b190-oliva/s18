
// function miniActivity(){
// 	let nickName = prompt("Please enter a value");
// 	console.log(nickName);
// }
// miniActivity();

function printName(name){
	console.log("Hello " +name);
}
printName("Bardok"); //- this values are called argument

/*
	Parameter - it acts as a named variable/container that exists only inside of a function
*/
printName("Bengbong");
printName("Bugembong");

let sampleVar = "Yua";
printName(sampleVar);

function miniActivity(parameter1, parameter2){
	console.log("The number passed as arguments are:");
	console.log("1. "+parameter1);
	console.log("2. "+parameter2);
};
miniActivity(98,99);

function miniActivity2(parameter3, parameter4, parameter5){
	console.log("My frineds are:");
	console.log("1. "+parameter3)
	console.log("2. "+parameter4);
	console.log("3. "+parameter5);
};
miniActivity2("Bardok", "Bengbong", "Bugembong");

function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of " +num+ " is "+remainder);
	let isDivisibleBy8 = remainder===0;
	console.log("Is "+num+ " divisible by 8?")
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);

// We can also do this above code using prompt(); 
// However, take note that using prompt() outputs a string data type.
// Strings data type are not ideal and cannot be used for mathematical computations.


function checkDivisibilityBy4(num){
	let remainder = num%4;
	console.log("The remainder of " +num+ " is "+remainder);
	let isDivisibleBy4 = remainder===0;
	console.log("Is "+num+ " divisible by 4?")
	console.log(isDivisibleBy4);
}

checkDivisibilityBy4(128);

// Function debugging practice

function isEven(num){
	let remainder = num%2;
	let divisible = remainder === 0;
	console.log(divisible); 
};

isEven(97);

function isOdd(num1){
	let remainder = num1%2;
	let divisible = remainder !== 0;
	console.log(divisible);
};

isOdd(44);

//Function as arguments
// function parameters can also accept other function as arguments

function argumentFunction(){
	console.log("This function is passed into another function");
};
function invokeFunction(functionParameter){
	functionParameter();
}

invokeFunction(argumentFunction);


//

let fName = "Juan";
let mName = "Dela";
let lName = "Cruz";
console.log(fName+" "+mName+" "+lName);

//return statement

function returnFullName(fName,mName,lName){
	return fName+" "+mName+" "+lName;
	// console.log("This message will not be printed");
}
console.log(returnFullName("Jeffrey", "Smnith", "Bezos"));

let completeName = returnFullName("Jeffrey", "Smnith", "Bezos");
console.log(completeName);

//mini activity

function returnAddress(address,city){
	return address +", "+ city;
};
console.log(returnAddress("Balangkas","Valenzuela"));

function printPlayerInfo(userName,level,job){
	console.log("Username: "+userName);
	console.log("Level: "+ level);
	console.log("Job: "+ job);
}

let user1 = printPlayerInfo("Knight_white",95,"Assasin Cross");
console.log(user1);
//returns undefined value because printPlayerInfo returns nothing, task only to print the messages in the console.
